#!/usr/bin/env python3.6
"""
requirement:
pip install facebook-sdk
"""

# Facebook SDK
import sys
import facebook
#spec.loader.exec_module(facebook)

short_token = str(sys.argv[1])
idPage = str(sys.argv[2])
# client_id y client_secret obtenidos de https://developers.facebook.com/apps/432137857642024/settings/basic/
client_id = "432137857642024"
client_secret = "1242b9862495d11abae7c68b56604a3c"

# Init graphAPI with short-lived token
graph = facebook.GraphAPI(short_token)

# Exchange short-lived-token to long-lived
long_token = graph.extend_access_token(client_id, client_secret)

# Init graphAPI with long-lived token
graph = facebook.GraphAPI(long_token['access_token'])

# Request all pages for user
pages = graph.get_object('me/accounts')


for page in pages['data']:
    if page['id'] == idPage:
        print(page['access_token'])
        break