var tokens = [];

window.fbAsyncInit = function () {
	FB.init({
		appId: '432137857642024',
		xfbml: true,
		version: 'v3.3'
	});
};

(function (d, s, id) {
	var js, fjs = d.getElementsByTagName(s)[0];
	if (d.getElementById(id)) { return; }
	js = d.createElement(s); js.id = id;
	js.src = "//connect.facebook.net/en_US/sdk.js";
	fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

var selectpages = document.getElementById('selectpages');
var selectforms = document.getElementById('selectforms');
var checkactivation = document.getElementById('activeCheck');
var usertoken = 'abc';
var pagetoken;

selectpages.addEventListener('change', function (event) { //ESCUCHA A QUE SE SELECCIONE UNA PAGINA DEL SELECT
	var value = selectpages.options[selectpages.selectedIndex].value;
	var token = tokens[selectpages.selectedIndex];
	pagetoken = token;
	var cadena = value + '/leadgen_forms';
	for (var i = 0, len = selectforms.length; i < len; i++) {//LIMPIA EL SELECT DE FORMS, LO VACIA
		selectforms.remove(i);
	}

	FB.api(cadena, { access_token: token }, //SE USA EL TOKEN DE ACCESO DE LA PAGE SELECCIONADA
		function (response) {
			console.log('Successfully retrieved forms', response);
			var forms = response.data;

			for (var i = 0, len = forms.length; i < len; i++) {//POBLA EL SELECT DE LOS FORMS
				var form = forms[i];
				var option = document.createElement('option');
				option.appendChild(document.createTextNode(form.name));
				option.value = form.id;
				selectforms.appendChild(option);
			}
		});
});


function registrarZap() {
	var name = document.getElementById('nombreZap').value;
	var tsource = document.getElementById('tSource').value;
	var status = checkactivation.checked ? 1 : 0;
	var idPage = selectpages.options[selectpages.selectedIndex].value;
	var idForm = selectforms.options[selectforms.selectedIndex].value;
	token = usertoken;
	sendZap2DB(name, tsource, status, token, idPage, idForm);
	$("#successModal").modal()
}

//FUNCION DE LOGIN DE FACEBOOK
function myFacebookLogin() {
	FB.login(function (response) {
		console.log('Successfully logged in', response);
		usertoken = response.authResponse.accessToken;
		FB.api('me/accounts', function (response) {
			console.log('Successfully retrieved pages', response);

			var pages = response.data;

			for (var i = 0, len = pages.length; i < len; i++) {//POBLA EL SELECT DE LAS PAGES
				var page = pages[i];
				var option = document.createElement('option');
				option.appendChild(document.createTextNode(page.name));
				option.value = page.id;
				tokens[i] = page.access_token; //OBTIENE LOS ACCESS TOKEN DE CADA PAGINA
				selectpages.appendChild(option);
			}
		});
	}, { scope: 'manage_pages, leads_retrieval' }); //PERMISOS SOLICITADOS Y NECESARIOS PARA ACCEDER A LA INFO DE LAS PAGES, FORMS Y LEADS
}

//ENVIA ZAP A LA DB
function sendZap2DB(name, tsource, status, token, idPage, idForm) {
	Url = 'api/addzap.php';
	var name = name;
	var tsource = tsource;
	var status = status;
	var token = token;
	var idPage = idPage;
	var idForm = idForm;
	var xhr = new XMLHttpRequest();
	xhr.open("POST", Url, true);
	xhr.setRequestHeader('Content-Type', 'application/json');
	var json = JSON.stringify({
		name: name,
		tsource: tsource,
		status: status,
		token: token,
		idPage: idPage,
		idForm: idForm
	});
	console.log(json);
	xhr.send(json);
}
