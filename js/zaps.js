window.fbAsyncInit = function () {
    FB.init({
        appId: '432137857642024',
        xfbml: true,
        version: 'v3.3'
    });
};

(function (d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) { return; }
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));


var zapTable = document.getElementById('dataTable').getElementsByTagName('tbody')[0];
var selectedIndex = 0;
var selectedZapId = 0;

$(function(){
    populateZapList();
});

function populateZapList() {
    const Http = new XMLHttpRequest();
    const url = 'api/getzap.php';
    Http.open("GET", url);
    Http.send();
    Http.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            var jsonData = JSON.parse(Http.responseText);
            for (var i = 0; i < jsonData.items.length; i++) {
                loadZapToTable(jsonData.items[i], i);
            }
            $('#dataTable').DataTable();
        }
    }

}

function loadZapToTable(input, index) {
    var item = input;
    // Inserta una fila en la tabla, en la ultima posicion
    var zapRow = zapTable.insertRow(zapTable.rows.length);
    zapRow.onclick = function () {
        selectedIndex = index;
        selectedZapId = item.id;
        $('#modalNameInput').val(item.name);
        $('#modalTSourceInput').val(item.tsource);
        if(item.status == 1){
            $('#modalStatusInput').prop('checked', true);
        }
        else{
            $('#modalStatusInput').prop('checked', false);
        }
    }

    // Inserta celdas en la fila nueva
    var idCell = zapRow.insertCell(0);
    var nombreCell = zapRow.insertCell(1);
    var tsourceCell = zapRow.insertCell(2);
    var statusCell = zapRow.insertCell(3);
    var actionsCell = zapRow.insertCell(4);
    var tokenCell = zapRow.insertCell(5);

    // Adjunta el texto correspondiente a nombre y tsource del zap de la fila
    var idText = document.createTextNode(item.id);
    var nombreText = document.createTextNode(item.name);
    var tsourceText = document.createTextNode(item.tsource);
    var tokenText = document.createTextNode(item.token);

    // Arma el elemento del checkbox
    var iconcheck = document.createElement('i');
    var att = document.createAttribute("class");
    if (item.status == 1) {
        att.value = "fas fa-eye";
    }
    else {
        att.value = "fas fa-eye-slash";
    }
    iconcheck.setAttributeNode(att);

    // Arma boton de borrado
    var botondelete = document.createElement('button');
    att = document.createAttribute("data-toggle");
    att.value = "modal";
    botondelete.setAttributeNode(att);
    att = document.createAttribute("href");
    att.value = "#deleteConfirmModal";
    botondelete.setAttributeNode(att);
    att = document.createAttribute("type");
    att.value = "button";
    botondelete.setAttributeNode(att);
    att = document.createAttribute("class");
    att.value = "btn btn-outline-danger";
    botondelete.setAttributeNode(att);
    image = document.createElement('i');
    att = document.createAttribute("class");
    att.value = "fa fa-trash";
    image.setAttributeNode(att);
    botondelete.appendChild(image);

    // Arma boton de actualizacion
    var botonupdate = document.createElement('button');
    att = document.createAttribute("data-toggle");
    att.value = "modal";
    botonupdate.setAttributeNode(att);
    att = document.createAttribute("href");
    att.value = "#updateModal";
    botonupdate.setAttributeNode(att);
    att = document.createAttribute("type");
    att.value = "button";
    botonupdate.setAttributeNode(att);
    att = document.createAttribute("class");
    att.value = "btn btn-outline-warning";
    botonupdate.setAttributeNode(att);
    image = document.createElement('i');
    att = document.createAttribute("class");
    att.value = "fa fa-edit";
    image.setAttributeNode(att);
    botonupdate.appendChild(image);

    // Agrega a las celdas sus elementos correspondientes
    idCell.appendChild(idText);
    idCell.style.display = "none"; // oculta la celda que contiene el id
    nombreCell.appendChild(nombreText);
    tsourceCell.appendChild(tsourceText);
    statusCell.appendChild(iconcheck);
    /*actionsCell.appendChild(botonplay);*/
    actionsCell.appendChild(botonupdate);
    actionsCell.appendChild(botondelete);    
    tokenCell.appendChild(tokenText);
    tokenCell.style.display = "none"; //oculta la celda que contiene el token
}

function editZap() {
    Url = 'api/addzap.php';
    var xhr = new XMLHttpRequest();
    xhr.open("POST", Url, true);
    xhr.setRequestHeader('Content-Type', 'application/json');
    var id = selectedZapId;
    var name = $('#modalNameInput').val();
    var tsource = $('#modalTSourceInput').val();
    var status;
    if($('#modalStatusInput').prop("checked")){
        status = 1;
    }
    else{
        status = 0;
    }
    xhr.send(JSON.stringify({
        id: id,
        name: name,
        tsource: tsource,
        status: status
    }));
    zapTable.rows[selectedIndex].cells[1].innerHTML = name;
    zapTable.rows[selectedIndex].cells[2].innerHTML = tsource;
    // Arma el elemento del checkbox
    var iconcheck = document.createElement('i');
    var att = document.createAttribute("class");
    if (status == 1) {
        att.value = "fas fa-eye";
    }
    else {
        att.value = "fas fa-eye-slash";
    }
    iconcheck.setAttributeNode(att);
    //borra el ojo anterior
    while (zapTable.rows[selectedIndex].cells[3].firstChild) {
        zapTable.rows[selectedIndex].cells[3].removeChild(zapTable.rows[selectedIndex].cells[3].firstChild);
    }
    //setea el nuevo
    zapTable.rows[selectedIndex].cells[3].appendChild(iconcheck);
}


function removeZap() {
    Url = 'api/addzap.php';
    var xhr = new XMLHttpRequest();
    xhr.open("POST", Url, true);
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.send(JSON.stringify({
        id: selectedZapId
    }));
    zapTable.deleteRow(selectedIndex);
}

//ENVIA LEAD A LA DB
function sendLead2DB(id, created_time, info, idZap) {
    Url = 'api/addlead.php';
    var id = id;
    var created_time = created_time;
    var info = info;
    var idZap = idZap;
    var xhr = new XMLHttpRequest();
    xhr.open("POST", Url, true);
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.send(JSON.stringify({
        id: id,
        created_time: created_time,
        info: info,
        idZap: idZap
    }));
}