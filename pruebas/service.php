<?php
include_once '../config.php';
require_once '../api/api.php';
require_once '/home/tomassilgueira/vendor/autoload.php';
    $api = new Api();
    //TRAE DE FB LOS LEADS
    //TRAE LOS DRIVERS DE LA DB
    $drivers = $api->getAllDrivers()["items"];

    foreach ($drivers as $driver) {
        if ($driver["active"] == "1") {
        $fb = new Facebook\Facebook([
            'app_id' => FB_APP_ID,
            'app_secret' => FB_APP_SECRET,
            'default_graph_version' => 'v3.3',
        ]);
        try {
        
            $response = $fb->get($driver["fb_form_id"].'/leads', $driver["fb_token"]);       
            
        } catch(\Facebook\Exceptions\FacebookResponseException $e) {
            // When Graph returns an error
            echo 'Graph returned an error: ' . $e->getMessage();
            exit;
        } catch(\Facebook\Exceptions\FacebookSDKException $e) {
            // When validation fails or other local issues
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            exit;
        }
        $feedEdge = $response->getGraphEdge();
        
        //ENVIA A LA DB LOS LEADS
        do {
            foreach ($feedEdge as $lead) {
            $leadArray = $lead->asArray();
            $leadArray["created_time"]->setTimezone(new DateTimeZone('America/Asuncion'));
            $created_at = $leadArray["created_time"]->format('d/m/Y H:i:sP');
            $lead_id = $leadArray["id"];
            $updated_at = $created_at;
            $sent_at = "-";
            $service_id = "a";//FALTA DEFINIR
            $data = "";
            for ($i = 0; $i < sizeof($leadArray["field_data"]); $i++) {
                $infoUnit = $leadArray["field_data"][$i];
                $data = $data . $infoUnit["name"] . "=" . $infoUnit["values"][0] . ";"; 
                
            }   
            $data = substr($data, 0, -1); //elimina ultimo char que es un salto de linea
            $driver_id = $driver["driver_id"];
            $item = array(
                'lead_id' => $lead_id,
                'created_at' => $created_at,
                'data' => $data,
                'driver_id' => $driver_id,
                'updated_at' => $updated_at,
                'sent_at' => $sent_at,
                'service_id' => $service_id
            );
            $api->addLead($item);     
            }
            $feedEdge = $fb->next($feedEdge);
        } while ($feedEdge);
    }        
}
?>