CREATE TABLE ZAP(

id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
name Varchar(30),
tsource Varchar(40),
status Boolean DEFAULT false,
token Varchar(300),
idForm Varchar(30)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci COMMENT='tabla de zaps';
%%%
ALTER TABLE `ZAP` ADD COLUMN `created_at` VARCHAR (50) DEFAULT '';
ALTER TABLE `ZAP` ADD COLUMN `updated_at` VARCHAR (50) DEFAULT '';
ALTER TABLE `ZAP` ADD COLUMN `type` VARCHAR (20) DEFAULT '';
alter table ZAP change status active BOOLEAN;
alter table ZAP change token fb_token Varchar(300);
alter table ZAP change idForm fb_form_id Varchar(30);
alter table ZAP change name description Varchar(30);
alter table ZAP change id driver_id int NOT NULL AUTO_INCREMENT;
ALTER TABLE ZAP DROP COLUMN tsource;
rename table ZAP to DRIVER;
ALTER TABLE DRIVER ADD COLUMN provider_id Varchar(30);
ALTER TABLE DRIVER ADD FOREIGN KEY (provider_id) REFERENCES PROVIDER(provider_id);


CREATE TABLE LEAD(
id Varchar(30) PRIMARY KEY NOT NULL,
created_time Varchar(50),
info Varchar(1200),
idZap int NOT NULL,
FOREIGN KEY (idZap) REFERENCES ZAP(id)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci COMMENT='tabla de leads';
%%%
ALTER TABLE `LEAD` ADD COLUMN `updated_at` VARCHAR (50) DEFAULT '';
ALTER TABLE `LEAD` ADD COLUMN `sent_at` VARCHAR (50) DEFAULT '';
ALTER TABLE `LEAD` ADD COLUMN `status` VARCHAR (10) DEFAULT 'PENDIENTE';
alter table LEAD change id lead_id Varchar(30) NOT NULL;
alter table LEAD change info data Varchar(1200);
alter table LEAD change created_time created_at Varchar(50);
alter table LEAD change idZap driver_id int NOT NULL;
ALTER TABLE LEAD ADD COLUMN service_id Varchar(30);
ALTER TABLE LEAD ADD FOREIGN KEY (service_id) REFERENCES SERVICE(service_id);
ALTER TABLE LEAD DROP COLUMN status;


########################

CREATE TABLE DRIVER(
driver_id Varchar(30) PRIMARY KEY NOT NULL,
description Varchar(50),
active Boolean DEFAULT true,
created_at Varchar(50) DEFAULT '',
updated_at Varchar(50) DEFAULT '',
fb_token Varchar(300),
fb_form_id Varchar(30),
type Varchar(30) DEFAULT ''
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci COMMENT='tabla de drivers (zaps de antes)';

CREATE TABLE PROVIDER(
provider_id Varchar(30) PRIMARY KEY NOT NULL,
description Varchar(50),
active Boolean DEFAULT true,
created_at Varchar(50),
updated_at Varchar(50) DEFAULT ''
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci COMMENT='tabla de proveedores';

CREATE TABLE USER(
user_id Varchar(30) PRIMARY KEY NOT NULL,
active Boolean DEFAULT true,
created_at Varchar(50),
updated_at Varchar(50) DEFAULT '',
password Varchar(30),
email Varchar(50)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci COMMENT='tabla de usuarios';

CREATE TABLE CLIENT(
client_id Varchar(30) PRIMARY KEY NOT NULL,
description Varchar(50),
active Boolean DEFAULT true,
created_at Varchar(50),
updated_at Varchar(50) DEFAULT '',
ip Varchar(30),
port Varchar(10),
tech Varchar(10),
protocol Varchar(30),
username Varchar(30),
password Varchar(30)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci COMMENT='tabla de clientes';

CREATE TABLE SERVICE(
service_id Varchar(30) PRIMARY KEY NOT NULL,
description Varchar(50),
active Boolean DEFAULT true,
created_at Varchar(50),
updated_at Varchar(50) DEFAULT '',
driver_id int,
client_id Varchar(30),
FOREIGN KEY (driver_id) REFERENCES DRIVER(driver_id),
FOREIGN KEY (client_id) REFERENCES CLIENT(client_id)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci COMMENT='tabla de servicios';

CREATE TABLE LOG(
log_id Varchar(30) PRIMARY KEY NOT NULL,
created_at Varchar(50),
actions Varchar(50),
info Varchar(50),
response_text Varchar(50),
response_http_co Varchar(50),
ip Varchar(50),
port Varchar(50),
information Varchar(50),
service_id Varchar(30),
FOREIGN KEY (service_id) REFERENCES SERVICE(service_id)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci COMMENT='tabla de logs';

CREATE TABLE LEAD(
lead_id Varchar(30) PRIMARY KEY NOT NULL,
data Varchar(1200),
created_at Varchar(50),
updated_at Varchar(50),
status Varchar(30),
sent_at Varchar(50),
driver_id Varchar(30),
service_id Varchar(30),
client_id Varchar(30),
provider_id Varchar(30),
FOREIGN KEY (driver_id) REFERENCES DRIVER(driver_id),
FOREIGN KEY (service_id) REFERENCES SERVICE(service_id),
FOREIGN KEY (client_id) REFERENCES CLIENT(client_id),
FOREIGN KEY (provider_id) REFERENCES PROVIDER(provider_id)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci COMMENT='tabla de leads';