<?php
include_once '../config.php';
class DB{
    private $servername;
    private $dbname;
    private $username;
    private $password;
    public function __construct(){
        $this->servername     = SERVER_NAME;
        $this->dbname       = DB_NAME;
        $this->username     = USER_NAME;
        $this->password = PASS_DB;
    }
    //mysql -e "USE todolistdb; select*from todolist" --user=azure --password=6#vWHD_$ --port=49175 --bind-address=52.176.6.0
    function connect(){
    
        try{
            
            $connection = "mysql:host=".$this->servername.";dbname=" . $this->dbname .';charset=utf8';
            $options = [
                PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
                PDO::ATTR_EMULATE_PREPARES   => false,
            ];
            //$pdo = new PDO($connection, $this->user, $this->password, $options);
            $pdo = new PDO($connection,$this->username,$this->password);
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            return $pdo;
        }catch(PDOException $e){
            print_r('Error connection: ' . $e->getMessage());
        }   
    }
}
?>