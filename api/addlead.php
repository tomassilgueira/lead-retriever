<?php
    include_once 'api.php';
    $json_data = file_get_contents('php://input');
    $data = json_decode($json_data, true);
    var_dump($data);
    $api = new Api();

    if(isset($data['id']) && isset($data['created_time']) && isset($data['info']) && isset($data['idZap'])){
        $item = array(
            'id' => $data['id'],
            'created_time' => $data['created_time'],
            'info' => $data['info'],
            'idZap' => $data['idZap']
        );
        $api->addLead($item);
    }else{
        $api->error('Error al llamar a la API');
    }
    
?>