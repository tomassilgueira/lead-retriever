<?php
include_once 'models.php';

class Api{
    //Lead functions
    function getAllLeads(){
        $lead = new Lead();
        $leads = array();
        $leads["items"] = array();
        $res = $lead->obtenerLeads();
        if($res->rowCount()){
            while ($row = $res->fetch(PDO::FETCH_ASSOC)){
    
                $item=array(
                    "id" => $row['id'],
                    "created_time" => $row['created_time'],
                    "info" => $row['info'],
                    "idZap" => $row['idZap'],
                );
                array_push($leads["items"], $item);
            }
            //var_dump($leads);
            //exit;
            $this->printJSON($leads);

        }else{

            $this->error("No hay leads registrados.");
        }
    }

    function getLeadById($id){
        $lead = new Lead();
        $leads = array();
        $leads["items"] = array();
        $res = $lead->obtenerLead($id);
        if($res->rowCount() == 1){
            $row = $res->fetch();
    
            $item=array(
                "id" => $row['id'],
                "created_time" => $row['created_time'],
                "info" => $row['info'],
                "idZap" => $row['idZap'],
            );
            array_push($leads["items"], $item);
        
            $this->printJSON($leads);

        }else{

            $this->error("No hay elementos.");
        }
    }

    function addLead($item){
        $lead = new Lead();
        $res = $lead->nuevoLead($item);
        $this->exito('Nuevo lead registrado');
    }

    //Driver functions
    function getAllDrivers(){
        $driver = new Driver();
        $drivers = array();
        $drivers["items"] = array();
        $res = $driver->obtenerDrivers();
        if($res->rowCount()){
            while ($row = $res->fetch(PDO::FETCH_ASSOC)){
    
                $item=array(
                    "driver_id" => $row['driver_id'],
                    "description" => $row['description'],
                    "active" => $row['active'],
                    "fb_token" => $row['fb_token'],
                    "fb_form_id" => $row['fb_form_id'],
                    "created_at" => $row['created_at'],
                    "updated_at" => $row['updated_at'],
                    "type" => $row['type'],
                    "provider_id" => $row['provider_id'],
                );
                array_push($drivers["items"], $item);
            }
            //var_dump($drivers);
            //exit;
            //$this->printJSON($drivers);
            return $drivers;
        }else{

            $this->error("No hay drivers registrados.");
        }
    }

    function getDriverById($driver_id){
        $driver = new Driver();
        $drivers = array();
        $drivers["items"] = array();
        $res = $driver->obtenerDriver($driver_id);
        if($res->rowCount() == 1){
            $row = $res->fetch();
    
            $item=array(
                "driver_id" => $row['driver_id'],
                "description" => $row['description'],
                "active" => $row['active'],
                "fb_token" => $row['fb_token'],
                "fb_form_id" => $row['fb_form_id'],
                "created_at" => $row['created_at'],
                "updated_at" => $row['updated_at'],
                "type" => $row['type'],
                "provider_id" => $row['provider_id'],
            );
            array_push($drivers["items"], $item);
        
            $this->printJSON($drivers);

        }else{

            $this->error("No hay elementos.");
        }
    }

    function addDriver($item){
        $driver = new Driver();
        $res = $driver->nuevoDriver($item);
        $this->exito('Nuevo driver registrado');
    }

    function updateDriver($driver_id, $description, $active, $fb_token, $fb_form_id, $updated_at, $type, $provider_id){
        $driver = new Driver();
        $res = $driver->modificarDriver($driver_id, $description, $active, $fb_token, $fb_form_id, $updated_at, $type, $provider_id);
        $this->exito('Driver con id '.$driver_id.' actualizado.');
    }

    function removeDriver($driver_id){
        $driver = new Driver();
        $res = $driver->borrarDriver($driver_id);
        $this->exito('Driver con id '.$driver_id.' eliminado.');
    }

    //Common functions
    function error($mensaje){
        echo json_encode(array('mensaje' => $mensaje)); 
    }
    function exito($mensaje){
        echo '<code>' . json_encode(array('mensaje' => $mensaje)) . '</code>'; 
    }
    function printJSON($array){
        echo /*'<code>'.*/json_encode($array, JSON_UNESCAPED_UNICODE)/*.'</code>'*/;
    }
}

?>