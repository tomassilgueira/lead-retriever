<?php
include_once 'db.php';
class User extends DB{
    
    function obtenerUsuarios(){
        $query = $this->connect()->query('SELECT * FROM USER');
        return $query;
    }
    function obtenerUsuario($user_id){
        $query = $this->connect()->prepare('SELECT * FROM USER WHERE user_id = :user_id');
        $query->execute(['user_id' => $user_id]);
        return $query;
    }
    function nuevoUser($user){
        $query = $this->connect()->prepare('INSERT INTO USER (user_id, active, created_at, updated_at, password, email) VALUES (:user_id, :active, :created_at, :updated_at, :password, :email)');
        $query->execute(['user_id' => $user['user_id'], 'active' => $user['active'], 'created_at' => $user['created_at'], 'updated_at' => $user['updated_at'], 'password' => $user['password'], 'email' => $user['email']]);
        return $query;
    }
}
class Lead extends DB{
    
    function obtenerLeads(){
        $query = $this->connect()->query('SELECT * FROM LEAD');
        return $query;
    }
    function obtenerLead($lead_id){
        $query = $this->connect()->prepare('SELECT * FROM LEAD WHERE lead_id = :lead_id');
        $query->execute(['lead_id' => $lead_id]);
        return $query;
    }
    function nuevoLead($lead){
        $query = $this->connect()->prepare('INSERT INTO LEAD (lead_id, created_at, data, driver_id, updated_at, sent_at, service_id) VALUES (:lead_id, :created_at, :data, :driver_id, :updated_at, :sent_at, :service_id)');
        $query->execute(['lead_id' => $lead['lead_id'], 'created_at' => $lead['created_at'], 'data' => $lead['data'], 'driver_id' => $lead['driver_id'], 'updated_at' => $lead['updated_at'], 'sent_at' => $lead['sent_at'], 'service_id' => $lead['service_id']]);
        var_dump($query);
        return $query;
    }
}
class Driver extends DB{
    
    function obtenerDrivers(){
        $query = $this->connect()->query('SELECT * FROM DRIVER');
        return $query;
    }
    function obtenerDriver($driver_id){
        $query = $this->connect()->prepare('SELECT * FROM DRIVER WHERE driver_id = :driver_id');
        $query->execute(['driver_id' => $driver_id]);
        return $query;
    }
    function borrarDriver($driver_id){
        $query = $this->connect()->prepare('DELETE FROM DRIVER WHERE driver_id = :driver_id');
        $query->execute(['driver_id' => $driver_id]);
        return $query;
    }
    function nuevoDriver($driver){
        $query = $this->connect()->prepare('INSERT INTO DRIVER (driver_id, description, active, fb_token, fb_form_id, created_at, updated_at, type, provider_id) VALUES (:driver_id, :description, :active, :fb_token, :fb_form_id, :created_at, :updated_at, :type, :provider_id)');
        $query->execute(['driver_id' => $driver['driver_id'], 'description' => $driver['description'], 'active' => $driver['active'], 'fb_token' => $driver['fb_token'], 'fb_form_id' => $driver['fb_form_id'],'created_at' => $driver['created_at'],'updated_at' => $driver['updated_at'],'type' => $driver['type'],'provider_id' => $driver['provider_id']]);
        return $query;
    }
    function modificarDriver($driver_id, $description, $active, $fb_token, $fb_form_id, $updated_at, $type, $provider_id){
        $query = $this->connect()->prepare('UPDATE DRIVER SET description = :description, active = :active, fb_token = :fb_token, fb_form_id = :fb_form_id, updated_at = :updated_at, type = :type, provider_id = :provider_id WHERE driver_id = :driver_id');
        $query->execute(['driver_id' => $driver_id, 'description' => $description, 'active' => $active, 'fb_token' => $fb_token, 'fb_form_id' => $fb_form_id, 'updated_at' => $updated_at, 'type' => $type, 'provider_id' => $provider_id]);
        return $query;
    }
}

?>