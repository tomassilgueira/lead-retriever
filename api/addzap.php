<?php
    include_once 'api.php';
    include_once '../config.php';
    $json_data = file_get_contents('php://input');
    $data = json_decode($json_data, true);
    var_dump($data);
    $api = new Api();
    if(isset($data['name']) && isset($data['tsource']) && isset($data['status']) && isset($data['token']) && isset($data['idPage']) && isset($data['idForm'])){
        //obtener token que no expira, se pasan el token de usuario y el id de la page al script de python
error_log('################################################');
        $command = escapeshellcmd(PYTHON_PATH . '/get_long_token.py '.$data['token'].' '.$data['idPage']);
error_log($command);
        $page_token = shell_exec($command); //token que no expira
        $item = array(
            'name' => $data['name'],
            'tsource' => $data['tsource'],
            'status' => $data['status'],
            'token' => $page_token,
            'idForm' => $data['idForm']
        );
        $api->addZap($item);
    }elseif(isset($data['id']) && isset($data['name']) && isset($data['tsource']) && isset($data['status'])){
        $id = $data['id'];
        $name= $data['name'];
        $tsource= $data['tsource'];
        $status= $data['status'];
        $api->updateZap($id, $name, $tsource, $status);
    }elseif(isset($data['id']) && isset($data['status'])){
        $id = $data['id'];
        $status = $data['status'];
        $api->updateZapStatus($id, $status);
    }elseif(isset($data['id'])){
        $id = $data['id'];
        $api->removeZap($id);
    }else{
        $api->error('Error al llamar a la API');
    }
    
?>
