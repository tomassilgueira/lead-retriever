<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css"
        integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css"
        integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
    <link rel="shortcut icon" type="image/x-icon" href="favicon.ico">
    <title>Zaps - Leads Retriever</title>
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <script src="js/jquery-3.4.1.min.js"></script>
    <script src="js/jquery.dataTables.min.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <style type="text/css">
        th {
            text-align: center;
        }
        body {
            font-family: 'Varela Round', sans-serif;
        }

        .modal-confirm {
            color: #636363;
            width: 550px;
        }

        .modal-confirm .modal-content {
            padding: 20px;
            border-radius: 5px;
            border: none;
        }

        .modal-confirm .modal-header {
            padding: 0 15px;
            border-bottom: none;
            position: relative;
        }

        .modal-confirm h4 {
            display: inline-block;
            font-size: 26px;
        }

        .modal-confirm .close {
            position: absolute;
            top: -5px;
            right: -5px;
        }

        .modal-confirm .modal-body {
            color: #999;
        }

        .modal-confirm .modal-footer {
            background: #ecf0f1;
            border-color: #e6eaec;
            text-align: right;
            margin: 0 -20px -20px;
            border-radius: 0 0 5px 5px;
        }

        .modal-confirm .btn {
            color: #fff;
            border-radius: 4px;
            transition: all 0.4s;
            border: none;
            padding: 8px 20px;
            outline: none !important;
        }

        .modal-confirm .btn-info {
            background: #b0c1c6;
        }

        .modal-confirm .btn-info:hover,
        .modal-confirm .btn-info:focus {
            background: #92a9af;
        }

        .modal-confirm .btn-danger {
            background: #f15e5e;
        }

        .modal-confirm .btn-danger:hover,
        .modal-confirm .btn-danger:focus {
            background: #ee3535;
        }

        .modal-confirm .modal-footer .btn+.btn {
            margin-left: 10px;
        }

        .trigger-btn {
            display: inline-block;
            margin: 100px auto;
        }
    </style>
</head>

<body>
    <!-- Navbar -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-primary sticky-top">
        <div class="container">
            <a class="navbar-brand" href="{% url 'home' %}">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                <ul class="navbar-nav">
                    <li class="nav-item mr-3">
                        <a class="nav-link" href="platform.html">
                            <i class="fas fa-plus-circle"></i>
                            REGISTRAR</a>
                    </li>

                    <li class="nav-item active mr-3">
                        <a class="nav-link" href="tabla.php">
                            <i class="fas fa-clipboard-list"></i>
                            DRIVERS</a>

                </ul>
            </div>
        </div>
    </nav>
    <br>
    <div class="container">
        <div class="row justify-content-center">
            <div class="form-group col-md-10 text-center">

                <div class="card shadow mb-4">
                    <div class="card-body">
                        <div class="table">
                            <table class="table table-bordered" id="dataTable" class="display" width="100%"
                                cellspacing="0">
                                <thead>
                                    <tr>
                                        <th style="display:none"></th>
                                        <th>DESCRIPCION</th>
                                        <th>TSOURCE</th>
                                        <th>ESTADO</th>
                                        <th>ACCIONES</th>
                                        <th style="display:none"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Delete Confirm Modal HTML -->
    <div id="deleteConfirmModal" class="modal fade">
        <div class="modal-dialog modal-confirm">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">¿Seguro que desea eliminar el zap?</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
                    <p>Todos los leads obtenidos con el serán eliminados de la base de datos.</p>
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn btn-info" data-dismiss="modal">Cancelar</a>
                    <a onclick="removeZap()" data-dismiss="modal" class="btn btn-danger">Sí</a>
                </div>
            </div>
        </div>
    </div>

    <!-- Zap Update Modal HTML -->
    <div id="updateModal" class="modal fade">
        <div class="modal-dialog modal-confirm">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Actualización de Zap</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
                    <p>Modifique los campos que desee del zap.</p>
                    <div class="form-group">
                        <label>Nombre</label>
                        <input type="text" id="modalNameInput" class="form-control" required="required" value="John" >
                    </div>
                    <div class="form-group">
                        <label>TSource</label>
                        <input type="text" id="modalTSourceInput" class="form-control" required="required">
                    </div>
                    <div class="form-group">
                        <label class="form-check-label" for="exampleCheck1">Estado activo</label>
                        <p></p>
                        <input type="checkbox" id="modalStatusInput" class="form-check-input" id="exampleCheck1">
                        <br>
                    </div>
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn btn-info" data-dismiss="modal">Cancelar</a>
                    <a onclick="editZap()" data-dismiss="modal" class="btn btn-danger">Confirmar</a>
                </div>
            </div>
        </div>
    </div>
    <script src="js/zaps.js?<?php echo time() ?>"></script>
</body>

</html>